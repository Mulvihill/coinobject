const coin = {
    state: 0,
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        coin.state = Math.round(Math.random());
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.

        if (coin.state === 0) {return "Heads"}
        else {return "Tails"};
    },
    toHTML: function() {
        const image = document.createElement('img');
        if (coin.state === 0) {
            image.src = "https://i2.wp.com/news.coinupdate.com/wp-content/uploads/2019/05/2019-quarter_W-Mintmark.jpg?resize=430%2C430";
            image.alt = "This is a picture of the face of a quarter, AKA heads"
            image.style = "width:100px;height:100px";
        }
        else {image.src = "//upload.wikimedia.org/wikipedia/commons/4/4e/COBREcentavosecuador2000-2.png"
        image.alt = "This is a picture of the back of a quarter, AKA tails." 
        image.style = "width:100px;height:100px";
        };
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        return image;
    }
};
function display20Flips() {
    const results = [];
    for (i = 0; i < 20; i++) {
        coin.flip();
        var x = coin.toString();
        results.push(x);
        document.write(x + ", ");
    }
    return results;
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
}
function display20Images() {
    const results = [];
    for (i=0;i<20;i++) {
        coin.flip();
        var p = coin.toHTML();
        document.body.appendChild(p);
        console.log(coin.state);
    }
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
}
display20Images();
display20Flips();